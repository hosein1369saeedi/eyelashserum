$(document).ready(function() {
    $(".menu-icon").on("click", function() {
          $("nav ul").toggleClass("showing");
    });
});

// Scrolling Effect

$(window).on("scroll", function() {
    if($(window).scrollTop()) {
          $('nav').addClass('black');
    }

    else {
          $('nav').removeClass('black');
    }
})


$(function() {
var header = $(".navbar");

      $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                  header.addClass("scrolled");
            } else {
                  header.removeClass("scrolled");
            }
      });

});

$('.carousel').carousel({
      interval: 3000
  });