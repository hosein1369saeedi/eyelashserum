<div class="team">
    <br>
    <br>
    <div class="container">
        <section class="our-webcoderskull padding-lg">
            <div class="">
                <ul class="row">
                    <li class="col-12 col-md-6 col-lg-3">
                        <a href="#">
                            <div class="cnt-block equal-hight">
                                <figure><img src="images/user.png" class="img-responsive" alt=""></figure>
                                <p>BeautyZone was extremely creative and forward thinking. they are aslo vere quick and efficient when executing changes for us.</p>
                                <h3 class="team__title">JOHN DOE</h3>
                                <span class="team__position">STUDENT</span>
                            </div>
                        </a>
                    </li>
                    <li class="col-12 col-md-6 col-lg-3">
                        <a href="#">
                            <div class="cnt-block equal-hight">
                                <figure><img src="images/user.png" class="img-responsive" alt=""></figure>
                                <p>BeautyZone was extremely creative and forward thinking. they are aslo vere quick and efficient when executing changes for us.</p>
                                <h3 class="team__title">JOANNA WANG</h3>
                                <span class="team__position">STUDENT</span>
                            </div>
                        </a>
                    </li>
                    <li class="col-12 col-md-6 col-lg-3">
                        <a href="#">
                            <div class="cnt-block equal-hight">
                                <figure><img src="images/user.png" class="img-responsive" alt=""></figure>
                                <p>BeautyZone was extremely creative and forward thinking. they are aslo vere quick and efficient when executing changes for us.</p>
                                <h3 class="team__title">ESPEN BRONBERG</h3>
                                <span class="team__position">STUDENT</span>
                            </div>
                        </a>
                    </li>
                    <li class="col-12 col-md-6 col-lg-3">
                        <a href="#">
                            <div class="cnt-block equal-hight">
                                <figure><img src="images/user.png" class="img-responsive" alt=""></figure>
                                <p>BeautyZone was extremely creative and forward thinking. they are aslo vere quick and efficient when executing changes for us.</p>
                                <h3 class="team__title">JOHN DOE</h3>
                                <span class="team__position">STUDENT</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <div class="button text-center">
            <button type="button" class="btn btn-success mt-4">Submit Your Review</button>
        </div>
    </div>
    <br>
</div>