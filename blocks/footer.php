<div class="footer">
    <div class="container">
        <br>
        <div class="menu-footer text-center">
            <ul>
                <li><a href="">HOME</a></li>
                <li><a href="">ABOUT US</a></li>
                <li><a href="">COMPARISON TABLE</a></li>
                <li><a href="">Features</a></li>
                <li><a href="">Contact Us</a></li>
            </ul>
        </div>
        <br>
        <div class="row center">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <p>
                    *With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                </p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1">
                        <span class="flaticon flaticon-placeholder"></span>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-11">
                        <h6>ADDRESS</h6>
                        <P>6701 Democracy Blvd,Suite300.USA</P>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1">
                        <span class="flaticon flaticon-smartphone"></span>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-11">
                        <h6>PHONE</h6>
                        <p>0080-123456(24/7 Support line)</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1">
                        <span class="flaticon flaticon-envelope"></span>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-11">
                        <h6>EMAIL</h6>
                        <p>info@example.com</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <div class="bottom text-center">
        <a href=""><h5>©2018EYELASHGROWTHPRODUCTSREVIEWS.COM</h5></a>
    </div>
</div>