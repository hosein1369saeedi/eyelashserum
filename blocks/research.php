<div class="research">
    <div class="parallax">
        <div class="img">
            <div class="container">
                <div class="row">
                    <div class="title">
                        <h1>Research</h1>
                        <img src="images/Research.png" alt="">
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="container">
            <div class="title"><h2>#1 Woolash - Eyelash Growth Serume</h2></div>
            <br>
            <div class="row">
                <div class="col-lg-4">
                    <img src="images/resaerch-11.jpg" alt="">
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h6><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h6><strong>Customer's Rate :</strong> 9.7/10</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <ul>
                        <li class="left"><h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5></li>
                        <li class="right"><h5><strong>Customer's Rate :</strong> 9.7/10</h5></li>
                    </ul> -->
                    <br>
                    <p>
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                    </p>
                    <div class="row button">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-success mt-4">Visite Woolash Website</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-dark mt-4">Woolash Reviews</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="woolash green">
                        <div class="title">
                            <h3>Woolash Pros</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="woolash red">
                        <div class="title">
                            <h3>Woolash cons</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
        <div class="container">
            <div class="title"><h2>#1 Woolash - Eyelash Growth Serume</h2></div>
            <br>
            <div class="row">
                <div class="col-lg-4">
                    <img src="images/resaerch-11.jpg" alt="">
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Customer's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <ul>
                        <li class="left"><h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5></li>
                        <li class="right"><h5><strong>Customer's Rate :</strong> 9.7/10</h5></li>
                    </ul> -->
                    <br>
                    <p>
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                    </p>
                    <div class="row button">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-success mt-4">Visite Woolash Website</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-dark mt-4">Woolash Reviews</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="woolash green">
                        <div class="title">
                            <h3>Woolash Pros</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="woolash red">
                        <div class="title">
                            <h3>Woolash cons</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
        <div class="container">
            <div class="title"><h2>#1 Woolash - Eyelash Growth Serume</h2></div>
            <br>
            <div class="row">
                <div class="col-lg-4">
                    <img src="images/resaerch-11.jpg" alt="">
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Customer's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <ul>
                        <li class="left"><h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5></li>
                        <li class="right"><h5><strong>Customer's Rate :</strong> 9.7/10</h5></li>
                    </ul> -->
                    <br>
                    <p>
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                    </p>
                    <div class="row button">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-success mt-4">Visite Woolash Website</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-dark mt-4">Woolash Reviews</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="woolash green">
                        <div class="title">
                            <h3>Woolash Pros</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="woolash red">
                        <div class="title">
                            <h3>Woolash cons</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
        <div class="container">
            <div class="title"><h2>#1 Woolash - Eyelash Growth Serume</h2></div>
            <br>
            <div class="row">
                <div class="col-lg-4">
                    <img src="images/resaerch-11.jpg" alt="">
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Customer's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <ul>
                        <li class="left"><h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5></li>
                        <li class="right"><h5><strong>Customer's Rate :</strong> 9.7/10</h5></li>
                    </ul> -->
                    <br>
                    <p>
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                    </p>
                    <div class="row button">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-success mt-4">Visite Woolash Website</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-dark mt-4">Woolash Reviews</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="woolash green">
                        <div class="title">
                            <h3>Woolash Pros</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="woolash red">
                        <div class="title">
                            <h3>Woolash cons</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
        <div class="container">
            <div class="title"><h2>#1 Woolash - Eyelash Growth Serume</h2></div>
            <br>
            <div class="row">
                <div class="col-lg-4">
                    <img src="images/resaerch-11.jpg" alt="">
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <span class="flaticon flaticon-circle"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <h5><strong>Customer's Rate :</strong> 9.7/10</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <ul>
                        <li class="left"><h5><strong>Laboratory Chemist's Rate :</strong> 9.7/10</h5></li>
                        <li class="right"><h5><strong>Customer's Rate :</strong> 9.7/10</h5></li>
                    </ul> -->
                    <br>
                    <p>
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                        With endless products on the market claiming to be the best eyelash enhancer, it can be hard to sift through hundreds of lash serum reviews to find an eyelash growth serum that works. Not every serum for lashes stacks up to the competition or lives up to its claims. 
                    </p>
                    <div class="row button">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-success mt-4">Visite Woolash Website</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-dark mt-4">Woolash Reviews</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="woolash green">
                        <div class="title">
                            <h3>Woolash Pros</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                                <span class="flaticon flaticon-tick"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="woolash red">
                        <div class="title">
                            <h3>Woolash cons</h3>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                                <span class="flaticon flaticon-close"></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                                <h4>germ tissue all natural uniqui nutrient</h4>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
        <br>
    </div>
    <!-- <div style="height:1000px;background-color:red;font-size:36px">
        Scroll Up and Down this page to see the parallax scrolling effect.
        This div is just here to enable scrolling.
        Tip: Try to remove the background-attachment property to remove the scrolling effect.
    </div> -->
</div>